var mainOne = document.getElementById('banner-site');
var main    = document.getElementById('banner-multi-site');
var code    = main.dataset.code;

var banner = []
// console.log(mainOne.dataset)

requestAjax(code);

function requestAjax(code) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "http://localhost:9000/server.php?code=" + code, true);
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      // one
      embedImage(JSON.parse(this.responseText));

      // many 
      embedManyImage(JSON.parse(this.responseText));
    }
  };
  xhttp.send();
}

/* one imgae */
function embedImage(response) {
  mainOne.innerHTML = "<div class='generate-banner'><img src='" + response[0].image + "'/></div>";
  doStyle(response[0]);
}

function doStyle(response) {
  cover = document.getElementsByClassName("generate-banner")[0];
  image = cover.getElementsByTagName('img')[0];
  image.style.width = response.size;
}


/* many image */
function embedManyImage(response) {
  if (response.length > 0) {
    response.forEach(function(val) {
      banner.push("<div class='generate-multi-banner'><img src='"+ val.image +"' width='"+ val.size +"' /></div>")
    })
  }

  main.innerHTML = banner
}

banner.push('<style type="text/css"> \
      #banner-site {background: #000;text-align: center;width: 100%;position: fixed;bottom: 0;} \
    </style>')
main.innerHTML = banner